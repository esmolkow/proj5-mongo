"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
#client = MongoClient('localhost', 327018)
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.acpdb
#print(client.list_database_names())
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    # Input cleaning not in rubric for this assignment.
    # Assuming all inputs are valid.
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist_km = request.args.get('brevet_dist_km', 999, type=float)
    begin_date = request.args.get('begin_date', 999, type=str)
    begin_time = request.args.get('begin_time', 999, type=str)
    start_iso = arrow.get(begin_date+' '+begin_time+' +0000', 'YYYY-MM-DD HH:mm Z').isoformat()

    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    open_time = acp_times.open_time(km, brevet_dist_km, start_iso)
    close_time = acp_times.close_time(km, brevet_dist_km, start_iso)
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route("/submit", methods=['POST'])
def submit():
    km = request.form.getlist("km")
    open_times = request.form.getlist("open")
    close_times = request.form.getlist("close")
    #data = []
    db.acpdb.delete_many({})
    for i in range(len(km)):
        if (km[i] and open_times[i] and close_times[i]):
            db.acpdb.insert_one({'km':km[i],'open':open_times[i],'close':close_times[i]})
    # test case to detect if there are no entries
    if len(km) == 0:
        return flask.render_template('404.html')
    else:
        return flask.redirect("/")

@app.route("/display")
def display():
    _items = db.acpdb.find()
    items = [item for item in _items]
    # test case to detect if there are no entries / is nothing there
    if len(items) == 0:
        return flask.render_template('404.html')
    else:
        return flask.render_template('display.html', items=items)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
